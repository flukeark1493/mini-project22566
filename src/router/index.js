import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Public',
      component: () => import('@/views/layouts/DefaultLayout.vue'),
      redirection:'/',
      children:[
        {
          path: '/',
          name: 'home',
          component: () => import('@/views/HomeView.vue'),
          redirection:'/',
        },
        {
          path: '/about',
          name: 'about',
          component: () => import('@/views/AboutView.vue')
        },
        {
          path: '/signup',
          name: 'signup',
          component: () => import('@/views/SignUp.vue')
        },
        {
          path: '/signin',
          name: 'signin',
          component: () => import('@/views/SignIn.vue')
        },
      ],
    },
    {
      path: "/dashboard",
      name: "dashboardlayout",
      component: () => import("@/views/layouts/DashboardLayout.vue"),
      redirection: "/admin",
      children: [
        {
        path: "/admin/dashboard",
        name: "dashboard",
        component: () => import("@/views/Dashboard.vue"),
        },
      ],
    },
  ],
});

export default router;
